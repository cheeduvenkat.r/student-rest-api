package com.hcl.student.Studentrestsvc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.student.Studentrestsvc.dao.StudentDAO;
import com.hcl.student.Studentrestsvc.model.Student;

@Service
public class StudentserviceImpl implements StudentService{
	@Autowired
	StudentDAO studentDao;

	@Override
	public List<Student> getAllStudents() {
		List<Student> Students = new ArrayList();
		try {
			Students=studentDao.getAllStudents();
		}catch(Exception e) {
			
		}
		return Students;
	}

}
