package com.hcl.student.Studentrestsvc.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="student")
public class Student {
	
	@Id
	@GeneratedValue( strategy= GenerationType.AUTO )
	Integer studentId;
	
	@Column
	@NotNull
	String studentName;
	@NotNull
	String studentEmail;
	String mobile;
	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer employeeId) {
		this.studentId = employeeId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String employeeName) {
		this.studentName = employeeName;
	}

	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String employeeEmail) {
		this.studentEmail = employeeEmail;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
