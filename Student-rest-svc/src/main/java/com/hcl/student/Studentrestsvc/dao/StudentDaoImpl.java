package com.hcl.student.Studentrestsvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.student.Studentrestsvc.mapper.StudentRowMapper;
import com.hcl.student.Studentrestsvc.model.Student;
@Repository
public class StudentDaoImpl implements StudentDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Student> getAllStudents() {
		String ALL_Student = "SELECT * FROM student";
		RowMapper<Student> studentRowMapper = new StudentRowMapper();
		return jdbcTemplate.query(ALL_Student, studentRowMapper);
	}

}
