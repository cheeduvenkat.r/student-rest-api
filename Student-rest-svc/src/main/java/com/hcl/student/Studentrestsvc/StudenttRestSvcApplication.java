package com.hcl.student.Studentrestsvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages= {"com.hcl"})
@EntityScan("com.hcl.Student.model")
public class StudenttRestSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudenttRestSvcApplication.class, args);
	}
}
