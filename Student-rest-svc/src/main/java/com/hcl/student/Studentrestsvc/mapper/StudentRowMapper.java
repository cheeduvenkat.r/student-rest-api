package com.hcl.student.Studentrestsvc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hcl.student.Studentrestsvc.model.Student;

public class StudentRowMapper implements RowMapper<Student> {

	@Override
	public Student mapRow(ResultSet row, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Student student = new Student();
		student.setStudentId(row.getInt("student_id"));
		student.setStudentEmail(row.getString("student_email"));
		student.setMobile(row.getString("mobile"));
		student.setStudentName(row.getString("student_name"));
		return student;
	}

}
