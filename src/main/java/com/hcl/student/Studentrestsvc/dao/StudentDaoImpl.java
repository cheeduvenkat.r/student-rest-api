package com.hcl.student.Studentrestsvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.student.Studentrestsvc.mapper.StudentRowMapper;
import com.hcl.student.Studentrestsvc.model.Student;
import com.hcl.student.Studentrestsvc.service.StudentserviceImpl;

@Repository
public class StudentDaoImpl implements StudentDAO {

	private final static Logger LOGGER = org.slf4j.LoggerFactory.getLogger(StudentDaoImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	// Student student = new Student();

	@Override
	public List<Student> getAllStudents() {
		String ALL_Student = "SELECT * FROM student";
		RowMapper<Student> studentRowMapper = new StudentRowMapper();
		return jdbcTemplate.query(ALL_Student, studentRowMapper);
	}

	@Override
	public Student getStudentById(Integer student_id) {
		RowMapper<Student> studentRowMapper = new StudentRowMapper();
		LOGGER.info("Student Id" + student_id);

		String student_By_Id = "SELECT * FROM student WHERE student_id= ? ";
		try {
			List<Student> studentlist =  jdbcTemplate.query(student_By_Id, new Object[] { student_id },
					studentRowMapper);
			
			if(studentlist.size() >= 1) {
				return studentlist.get(studentlist.size()-1);
			}
			
			//LOGGER.info("Student Name" + student.getStudentName());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return null;
		
	}

}
