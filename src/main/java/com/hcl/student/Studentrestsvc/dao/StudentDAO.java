package com.hcl.student.Studentrestsvc.dao;

import java.util.List;

import com.hcl.student.Studentrestsvc.model.Student;

public interface StudentDAO {

	List<Student> getAllStudents();

	Student getStudentById(Integer student_id);
		

}
