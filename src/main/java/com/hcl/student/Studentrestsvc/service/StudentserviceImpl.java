package com.hcl.student.Studentrestsvc.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.student.Studentrestsvc.controller.StudentController;
import com.hcl.student.Studentrestsvc.dao.StudentDAO;
import com.hcl.student.Studentrestsvc.model.Student;

@Service
public class StudentserviceImpl implements StudentService{
	
	private final static Logger LOGGER = org.slf4j.LoggerFactory.getLogger(StudentserviceImpl.class);

	
	@Autowired
	StudentDAO studentDao;

	@Override
	public List<Student> getAllStudents() {
		List<Student> Students = new ArrayList();
		try {
			Students=studentDao.getAllStudents();
		}catch(Exception e) {
			
		}
		return Students;
	}

	@Override
	public Student getStudentById(Integer student_id) {
		Student student=new Student();
		try {
			student=studentDao.getStudentById(student_id);
			LOGGER.info("Student Name : " + student.getStudentName());
		}
		catch(Exception e) {
			
		}
		return student;
	}

}
