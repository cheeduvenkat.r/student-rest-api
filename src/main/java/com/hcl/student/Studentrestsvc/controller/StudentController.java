package com.hcl.student.Studentrestsvc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.student.Studentrestsvc.model.Student;
import com.hcl.student.Studentrestsvc.service.StudentService;

@RestController
@RequestMapping(value = "/student")
public class StudentController {

	private final static Logger LOGGER = org.slf4j.LoggerFactory.getLogger(StudentController.class);

	@Autowired
	StudentService studentService;

	@GetMapping("/all")
	public List<Student> getAllStudents() {
		return studentService.getAllStudents();
	}

	@GetMapping("/{student_id}")
	public Student getStudentById(@PathVariable Integer student_id) {

		LOGGER.info("Student Id : " + student_id);
		return studentService.getStudentById(student_id);
	}

	@PostMapping("/add")
	public ResponseEntity<Void> addStudent(@RequestBody Student student) {

		return new ResponseEntity<Void>(HttpStatus.CREATED);

	}

}
